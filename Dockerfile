FROM python:3.9.5
MAINTAINER HAL24K "docker@hal24k.com"
ENV PYTHONUNBUFFERED=0

RUN apt-get update
# Copy install dependencies
RUN pip3 install --upgrade pip
RUN pip3 install numpy pylint pigar configobj>=5.0.6
RUN apt-get install -y scons
RUN curl https://baltocdn.com/helm/signing.asc | apt-key add -
RUN apt-get update --fix-missing
RUN apt-get install apt-transport-https --yes --fix-missing
RUN echo "deb https://baltocdn.com/helm/stable/debian/ all main" | tee /etc/apt/sources.list.d/helm-stable-debian.list
RUN apt-get update --fix-missing

# Create work dir, and copy all files
WORKDIR /src

# Copy requirements.txt and install dependencies top level
COPY requirements.txt /src

ARG PIP_TRUSTED_HOST=pipserver.dimension.ws
ARG PIP_EXTRA_INDEX_URL=https://hal_muser:Mkw5VxNDNxkOMYrIDqdAy645mLGvnqxW@pip.dimension.ws:443/
RUN pip3 install \
        --trusted-host ${PIP_TRUSTED_HOST} \
        --extra-index-url ${PIP_EXTRA_INDEX_URL} \
        -r requirements.txt
RUN pip3 install git+https://github.com/benoitc/gunicorn.git

# Copy app files to image
COPY ./src /src
ENV URL_PREFIX='/'
ENV PYTHONPATH=/src:/src/test230
ENV SERVER_HOST='0.0.0.0'
ENV SERVER_PORT=8000
EXPOSE 8000

RUN ls -lcrth && which python && echo $PYTHON_PATH
CMD [ "streamlit", "run", "--server.port", "80", "--server.address", "0.0.0.0", "app.py"]